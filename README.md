# plnc
C program to print the n'th line of a given file

### Arguments

$ plnc \<n\> \<file\>

\<n\>   Line to print to stdout

\<file\>  File to grab line from
