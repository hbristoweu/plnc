#include "plnc.h"

void posix_error(int code, char *msg){
	fprintf(stderr,"%s: %s\n", msg, strerror(code));
	exit(EXIT_FAILURE);
}

int main(int argc, char* argv[]){

	size_t buffer_size  = 0;
	char *fileaddr, *buffer  = malloc(buffer_size * sizeof(char));
	FILE *fp;
	ssize_t read;
	int targetline = 0, currline = 1, wc = 1;

	if (argc > 2){
		targetline = atoi(argv[1]);
		fileaddr = argv[2];
	} else
		posix_error(EINVAL, "Incomplete syntax");

	fp = fopen(fileaddr, "r");
	if (fp == NULL)
		posix_error(errno, "Cannot open file");

	while (((read = getline(&buffer, &buffer_size, fp)) != -1) && wc ){
		if (currline == targetline){
			fprintf(stdout, "%s", buffer);
			wc--;
		}

		currline++;
	}

	fflush(stdout);
	fclose(fp);
	free(buffer);

	if (wc)
		return(EXIT_FAILURE);

	return(EXIT_SUCCESS);

}

